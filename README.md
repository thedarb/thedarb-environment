# Automating my Linux environments.

Wanting my normal tools, scripts, and shell configuration across my linux desktops and utility boxes, it was time to automate.

Packages Being installed:

------

**Via apt:**

- zsh - Powerful, quick, extensible, POSIX like shell.
- git (via ansible-role-git - formerly geerlingguy.git)
- bash - Standard default shell on most Linux distributions. Listed, as when this expands to beyond just Linux, I'd like to make sure it's there.
- ksh - Because it was a damn good standard POSIX like shell before zsh, and I have a library of scripts written in it.
- fzf
- dirmngr
- htop
- nload
- iotop
- jq
- curl
- wget
- vim

**Via snap:**

- kubectl

**Via git:**

- zplug

**Zsh plugins via zplug (see ansible-role-zplug submodule):**

- plugins/git
- zplug/zplug
- zsh-users/zsh-completions
- zsh-users/zsh-syntax-highlighting
- zsh-users/zsh-autosuggestions
- zsh-users/zsh-history-substring-search
- rimraf/k
- plugins/kubectl
- romkatv/powerlevel10k # To die for prompt and theme for Zsh.
- lukechilds/zsh-nvm

** Configures Powerline10k wizard!!!**

------

**Testing via vagrant:**

```shell
vagrant up vagrant_zsh-test
pipenv run ansible-playbook \
  home/my_collection/thedarb-environment.yml \
  -i home/my_collection/inventory/vagrant.yml --extra-vars "username=bdarbro"
```
